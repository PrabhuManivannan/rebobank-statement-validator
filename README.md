* API Name   : rebobank-statement-validator

*API Information
```bash
    This rebobank-statement-validator is a microservice ,which has been developed to validate rebo bank customer statement and to find the invalid statement from csv ,xml resources. 
```
* Validation Rules
```bash
  Rabo Bank customer statement need to be validated based on below conditions
  
     * all transaction references should be unique
     * end balance needs to be validated with mutation
```

* Version  1.0


*Developer Notes :



```bash

* Clone the repository

git clone https://PrabhuManivannan@bitbucket.org/PrabhuManivannan/rebobank-statement-validator.git

 
```

```bash
* How to run?


cd rebobank-statement-validator
mvn spring-boot:run
```

This API can be accessed at `http://localhost:8081/rebo/api/validate/statement`.


Swagger API documentation views in `http://localhost:8081/swagger-ui.html`.

You may also package the application in the form of a jar and then run the jar file like so -

```bash
mvn clean package
java -jar target/rebobank-statement-validator-1.0.0-RELEASE.jar
```



 * Do you need support?
 
 Name : Prabhu M
 Email : prbhuformail@gmail.com
 Mobile : 9789439652
 

