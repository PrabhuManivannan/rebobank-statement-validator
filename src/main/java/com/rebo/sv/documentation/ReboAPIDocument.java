package com.rebo.sv.documentation;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@Configuration
@EnableSwagger2
public class ReboAPIDocument {

	@Value("${company}")
	String company;

	@Value("${company.url}")
	String companyURL;

	@Value("${company.email}")
	String companyEmail;

	@Value("${company.license.url}")
	String companylicenseUrl;
	
	
	@Value("${rebo.license.id}")
	String companylicenseID;

	@Value("${rebo.api.basepackage}")
	String apiVersion;

	@Value("${rebo.api.description}")
	String apiDescription;

	@Value("${rebo.api.title}")
	String apiTitle;

	@Value("${rebo.api.basepackage}")
	String apiBasePackage;

	@Value("${swagger.api.basepath}")
	String swaggerPath;
	
	
	
/*	@Bean
	public Docket api() {                
	    return new Docket(DocumentationType.SWAGGER_2)          
	      .select()
	      .apis(RequestHandlerSelectors.basePackage(apiBasePackage))
	      .paths(PathSelectors.ant(swaggerPath))
	      .build()
	      .apiInfo(apiInfo());
	}
	 
	private ApiInfo apiInfo() {
		
		List<ResponseMessage> li = new ArrayList<>();
		li.add(new ResponseMessageBuilder().code(500).message("Internal Server Error").responseModel(new ModelRef("Error"))
				.build());
		
	    return new ApiInfo(  apiTitle, apiDescription, apiVersion,companylicenseUrl,
	    			new Contact(company, companyURL, companyEmail), 
	    			companylicenseID, companylicenseUrl, Collections.emptyList());
	}*/
	

	@Bean
	public Docket productApi() {
		ArrayList<ResponseMessage> li = new ArrayList<>();
		li.add(new ResponseMessageBuilder().code(401).message("Resource File Not Available").responseModel(new ModelRef("Error")).build());
		li.add(new ResponseMessageBuilder().code(500).message("Internal Server Error").responseModel(new ModelRef("Error")).build());
		li.add(new ResponseMessageBuilder().code(403).message("Forbidden issue which denies the permission to load theresource").responseModel(new ModelRef("Error")).build());
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage(apiBasePackage))
				.paths(PathSelectors.regex(swaggerPath)).build().apiInfo(metaData()).globalResponseMessage(RequestMethod.POST, li);
	}

	private ApiInfo metaData() {
		Contact contact = new Contact(company, companyURL, companyEmail);
		ApiInfoBuilder builder = new ApiInfoBuilder();
		return builder.description(apiDescription).version(apiVersion).licenseUrl(companylicenseUrl).title(apiTitle)
				.contact(contact).build();
	}
}