package com.rebo.sv.parse.helper;

/**
 * 
 * @Author   	Prabhu M
 * @Description Class used to parse the XML file by using JAXB
 */

import java.io.File;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


import com.rebo.sv.api.exception.FileParserException;
import com.rebo.sv.model.ReboRootXML;
import com.rebo.sv.model.ReboStatement;

@Component("xmlHelper")
public class ReboXMLProcessHelper implements ReboStatementProcessHelper {
	private static final Logger logger = LoggerFactory.getLogger(ReboXMLProcessHelper.class);
	
	/** method used to parse the xml file
	 * @param file
	 */
	public List<ReboStatement> processStatement(File file) throws FileParserException {
		ReboRootXML rootXML;
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(ReboRootXML.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			rootXML = (ReboRootXML) unmarshaller.unmarshal(file);
			logger.info("successfully parsed the statement record from xml file");
		} catch (Exception exception) {
			logger.error("exception while parsing xml file "+exception);
			throw new FileParserException(exception);
		}
		return rootXML.getStatements();
	}


}
