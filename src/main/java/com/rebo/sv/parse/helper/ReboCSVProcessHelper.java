package com.rebo.sv.parse.helper;

/**
 * 
 * @Author   	Prabhu M
 * @Description Class used to parse the CSV file
 */

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.rebo.sv.api.exception.FileParserException;
import com.rebo.sv.model.ReboStatement;
import com.rebo.sv.utill.ReboConstants;


@Component("cvsHelper")
public class ReboCSVProcessHelper implements ReboStatementProcessHelper{
	
	private static final Logger LOG = LoggerFactory.getLogger(ReboCSVProcessHelper.class);

	/** method used to parse the csv file
	 * @param file
	 */
	@SuppressWarnings("resource")
	@Override
	public List<ReboStatement> processStatement(File file) throws FileParserException {

		CSVFormat csvFileFormat = CSVFormat.DEFAULT.withHeader(ReboConstants.FILE_HEADER_MAPPING);
		List<ReboStatement> transactions;

		FileReader fileReader = null;

		try {
			fileReader = new FileReader(file);
			LOG.info("parsing csv file");
			CSVParser csvParser = new CSVParser(fileReader, csvFileFormat);
			List<CSVRecord> csvRecords = csvParser.getRecords();
			

			transactions = csvRecords.stream().skip(1)
					.map(record -> new ReboStatement(Long.parseLong(record.get(ReboConstants.REFERAANCE_NO)), record.get(ReboConstants.ACCOUNT_NUMBER),
							record.get(ReboConstants.DESCR), new BigDecimal(record.get(ReboConstants.START_BALANCE)).setScale(2, BigDecimal.ROUND_HALF_UP),
							new BigDecimal(record.get(ReboConstants.MUTATION)).setScale(2, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP),
							new BigDecimal(record.get(ReboConstants.END_BALANCE)).setScale(2, BigDecimal.ROUND_HALF_UP)))
					.collect(Collectors.toList());

			LOG.info("successfully fetched the statement record from csv file");

		} catch (Exception exception) {

			LOG.error("csv file procession error", exception);
			throw new FileParserException(exception);
		} finally {
			try {
				if (fileReader != null)
					fileReader.close();

			} catch (IOException exception) {
				LOG.error("csv file parsing error", exception);
			}
		}

		return transactions;
	}
	
}
