package com.rebo.sv.parse.helper;

import java.io.File;
import java.util.List;

import com.rebo.sv.api.exception.FileParserException;
import com.rebo.sv.model.ReboStatement;

public interface ReboStatementProcessHelper {
	public List<ReboStatement> processStatement(File file) throws FileParserException ;

}
