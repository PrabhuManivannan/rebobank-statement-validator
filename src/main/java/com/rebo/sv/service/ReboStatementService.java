package com.rebo.sv.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.rebo.sv.api.exception.ReboServiceException;
import com.rebo.sv.model.ReboStatement;
/**
 * 
 * @Author   	Prabhu M
 * @Description Service Layer
 */

public interface ReboStatementService {
	
	public List<ReboStatement>  processStatement(MultipartFile file,String fileFormat,String filePath) throws ReboServiceException;

}
