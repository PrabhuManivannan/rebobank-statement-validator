package com.rebo.sv.service;
/**
 * 
 * @Author   	Prabhu M
 * @Description This service layer will choose the parser either csv or xml based on file extension
 */
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.rebo.sv.api.exception.ReboServiceException;
import com.rebo.sv.model.ReboStatement;
import com.rebo.sv.parse.helper.ReboStatementProcessHelper;
import com.rebo.sv.utill.FileUtil;
import com.rebo.sv.utill.ReboConstants;

@Service
public class ReboStatementServiceImpl implements ReboStatementService{
	public static final Logger logger = LoggerFactory.getLogger(ReboStatementServiceImpl.class);
	
	
	@Autowired
	@Qualifier("cvsHelper")
	ReboStatementProcessHelper reboCSVProcessHelper;
	
	@Autowired
	@Qualifier("xmlHelper")
	ReboStatementProcessHelper reboXMLProcessHelper;
	
	
	
	/**
	 * Parse the file and find the invalid statement by using file extension
	 * @param multipartFile , fileFormat
	 * @return List<ReboStatement>
	 */
	@Override
	public List<ReboStatement>  processStatement(MultipartFile multipartFile,String fileFormat,String filePath) throws ReboServiceException {
		
		List<ReboStatement> statements = new ArrayList<>();
		try {
			FileUtil.storeFile(multipartFile, filePath);
			Path fileAbsolutePath=Paths.get(filePath).toAbsolutePath().normalize();
			Path path = fileAbsolutePath.resolve(FileUtil.getFileName(multipartFile)).normalize();
			Resource resource = new UrlResource(path.toUri());
			
			File file=resource.getFile();
			
			if (ReboConstants.CSV_FORMAT.equalsIgnoreCase(fileFormat)) {   
				statements = reboCSVProcessHelper.processStatement(file);
			} else { 
				statements = reboXMLProcessHelper.processStatement(file);
			} 
			logger.info("successfully parsed the file ");
			statements = findInvalidStatements(statements);
		} catch (Exception se) {
			logger.error("excepton while processing file ", se);
			 throw new ReboServiceException(se);   
		           
		} 
		return statements;
	}
	
	
	/**
	 * To find the invalid statement
	 * @param statements
	 * @return List<ReboStatement>
	 */
	private List<ReboStatement> findInvalidStatements(List<ReboStatement> statements) {
		Set<ReboStatement> invalidSet=new HashSet<>();
		List<ReboStatement> invalidStatements = validateMutation(statements);
		List<ReboStatement> dublicateList =findDublicateByRefrence(statements);
		
		invalidSet.addAll(invalidStatements);
		invalidSet.addAll(dublicateList);
		
		
		return new ArrayList<>(invalidSet);
	}

	/**
	 * Filters the duplicate statement reference number
	 * @param statements
	 * @return List<ReboStatement>
	 */
	private List<ReboStatement> findDublicateByRefrence(List<ReboStatement> statements) {
			
		 List<ReboStatement> duplicates = statements.stream()
					.collect(Collectors.groupingBy(ReboStatement::getTransactionRef)).entrySet().stream()
					.filter(e -> e.getValue().size() > 1).flatMap(e -> e.getValue().stream()).collect(Collectors.toList());
	
		 return duplicates;
	}
	
	/**
	 * Filters the statement with mutation failure records
	 * @param statements
	 * @return List<ReboStatement>
	 */
	private List<ReboStatement> validateMutation(List<ReboStatement> statements) {
		return statements.stream().filter(statement -> !isValid(statement)).collect(Collectors.toList());
	}
	/**
	 * Filters the statement with mutation failure records
	 * @param statements
	 * @return boolean
	 */
	private boolean isValid(ReboStatement statements) {
		return (statements.getEndBalance().subtract(statements.getStartBalance())).compareTo(statements.getMutation())==0;
	}

}
