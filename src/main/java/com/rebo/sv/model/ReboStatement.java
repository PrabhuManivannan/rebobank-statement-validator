package com.rebo.sv.model;

/**
 * 
 * @author Prabhu M
 * @category This POJO object used to holds the CSV and XML data 
 */

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "record")
public class ReboStatement {
	
	private Long transactionRef;
	private String accountNumber;
	private String description;
	private BigDecimal startBalance;
	private BigDecimal mutation;
	private BigDecimal endBalance;
	
	
	public ReboStatement() {
	}

	public ReboStatement(Long transactionRef, String accountNumber, String description, BigDecimal startBalance, BigDecimal mutation,
			BigDecimal endBalance) {
		super();
		this.transactionRef = transactionRef;
		this.accountNumber = accountNumber;
		this.description = description;
		this.startBalance = startBalance;
		this.mutation = mutation;
		this.endBalance = endBalance;
	}
	
	@XmlAttribute(name = "reference")
	public Long getTransactionRef() {
		return transactionRef;
	}
	public void setTransactionRef(Long transactionRef) {
		this.transactionRef = transactionRef;
	}
	@XmlElement(name = "accountNumber")
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	@XmlElement(name = "description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@XmlElement(name = "startBalance")
	public BigDecimal getStartBalance() {
		return startBalance.setScale(2, BigDecimal.ROUND_HALF_UP);
	}
	public void setStartBalance(BigDecimal startBalance) {
		this.startBalance = startBalance;
	}
	@XmlElement(name = "mutation")
	public BigDecimal getMutation() { 
		return mutation.setScale(2, BigDecimal.ROUND_HALF_UP);
	}
	public void setMutation(BigDecimal mutation) {
		this.mutation = mutation;
	}
	@XmlElement(name = "endBalance")
	public BigDecimal getEndBalance() {
		return endBalance.setScale(2, BigDecimal.ROUND_HALF_UP);
	}
	public void setEndBalance(BigDecimal endBalance) {
		this.endBalance = endBalance;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountNumber == null) ? 0 : accountNumber.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((endBalance == null) ? 0 : endBalance.hashCode());
		result = prime * result + ((mutation == null) ? 0 : mutation.hashCode());
		result = prime * result + ((startBalance == null) ? 0 : startBalance.hashCode());
		result = prime * result + ((transactionRef == null) ? 0 : transactionRef.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReboStatement other = (ReboStatement) obj;
		if (accountNumber == null) {
			if (other.accountNumber != null)
				return false;
		} else if (!accountNumber.equals(other.accountNumber))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (endBalance == null) {
			if (other.endBalance != null)
				return false;
		} else if (!endBalance.equals(other.endBalance))
			return false;
		if (mutation == null) {
			if (other.mutation != null)
				return false;
		} else if (!mutation.equals(other.mutation))
			return false;
		if (startBalance == null) {
			if (other.startBalance != null)
				return false;
		} else if (!startBalance.equals(other.startBalance))
			return false;
		if (transactionRef == null) {
			if (other.transactionRef != null)
				return false;
		} else if (!transactionRef.equals(other.transactionRef))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ReboStatement [transactionRef=" + transactionRef + ", accountNumber=" + accountNumber + ", description="
				+ description + ", startBalance=" + startBalance + ", mutation=" + mutation + ", endBalance="
				+ endBalance + "]";
	}
	

}
