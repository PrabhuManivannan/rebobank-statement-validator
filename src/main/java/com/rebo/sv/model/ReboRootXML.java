package com.rebo.sv.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Prabhu M
 * @category Root xml mapped into this root object
 */
@XmlRootElement(name = "records")
public class ReboRootXML  {

	List<ReboStatement> statement;

	@XmlElement(name = "record")
	public List<ReboStatement> getStatements() {
		return statement;
	}

	public void setStatements(List<ReboStatement> statement) {
		this.statement = statement;
	}

}
