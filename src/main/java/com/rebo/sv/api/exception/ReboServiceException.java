package com.rebo.sv.api.exception;

/**
 * 
 * @author   	Prabhu M
 * @Description Custom Service Exception class which is used to handle the exception in service layer
 *  
 */

public class ReboServiceException extends Exception {

	private static final long serialVersionUID = 1L;

	public ReboServiceException(Throwable excption) {
		super(excption);
	}

}
