package com.rebo.sv.api.exception;


/**
 * 
 * @author   	Prabhu M
 * @Description Custom exception class which is used to handle the file parser exception 
 *  
 */
public class FileParserException extends Exception {

	private static final long serialVersionUID = 1L;

	public FileParserException(Throwable excption) {
		super(excption);
	}

}
