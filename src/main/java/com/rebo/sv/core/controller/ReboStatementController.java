package com.rebo.sv.core.controller;

/**
 * 
 * @Author   	Prabhu M
 * @Description This is the controller class which is used to hold all the rebo Statement api's  URI details 
 *              Also the request and response , URI details has been documented by using swagger api 
 *  
 */

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.rebo.sv.core.ReboResponse;
import com.rebo.sv.model.ReboStatement;
import com.rebo.sv.service.ReboStatementService;
import com.rebo.sv.utill.FileUtil;
import com.rebo.sv.utill.ReboConstants;
import com.rebo.sv.utill.ReboResponseDetails;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/rebo/api/validate/")
public class ReboStatementController {
	
	public static final Logger logger = LoggerFactory.getLogger(ReboStatementController.class);
	
	@Autowired
	ReboStatementService reboStatementService;
	
	@Value("${file.upload-dir}")
	private String filePath;
	
	@PostMapping(value = "statement", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(notes=ReboConstants.URI_ACCESS_INFO,consumes = MediaType.MULTIPART_FORM_DATA_VALUE,  produces =  MediaType.APPLICATION_JSON_VALUE,  value = ReboConstants.REBO_API_INFO, response = ReboResponse.class)
	public ResponseEntity<?> processCustomerStatement(@RequestParam(ReboConstants.TRANSACTION_FILE) MultipartFile trxFile){
		
		ReboResponse reboResponse = new ReboResponse();
		
		try {
			String fileFormat = FileUtil.getFileExtension(trxFile);    
			
			if(FileUtil.isValidFile(fileFormat)) {
				logger.info("processing  file -> {}",trxFile.getName());
				List<ReboStatement> statements=reboStatementService.processStatement(trxFile,fileFormat,filePath);
				reboResponse.setStatements(statements);
				reboResponse.setStatusCode(ReboResponseDetails.TRANSATION_SUCCESS.getCode());  // TODO :: Make as enum
				reboResponse.setStatusDescription(ReboResponseDetails.TRANSATION_SUCCESS.getStatusDescription());
			}else{
				logger.error("invalid file format");
				reboResponse.setStatusCode(ReboResponseDetails.INVALID_FILE.getCode());  // TODO :: Make as enum
				reboResponse.setStatusDescription(ReboResponseDetails.INVALID_FILE.getStatusDescription());
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reboResponse);
			}
			
			
		}catch(Exception ex) {
			logger.error("excepton while processing file", ex);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
		
		return ResponseEntity.status(HttpStatus.OK).body(reboResponse);
	}

}
