package com.rebo.sv.core;

/**
 * 
 * @Author   	Prabhu M
 * @Description Response class is used to hold the response details to provide the clients
 *  
 */

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.rebo.sv.model.ReboStatement;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(value = "Rebo Statement Response")
public class ReboResponse {
	
	@JsonProperty(value = "failureRecords")
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	List<ReboStatement> statements  = new ArrayList<>();
	
	@ApiModelProperty(allowableValues = "S001,E001")
	@JsonProperty(value = "statusCode")
	private String statusCode;

	
	@ApiModelProperty(allowableValues = "Found invalid records, Invalid file") 
	@JsonProperty(value = "statusDescription")
	private String statusDescription;

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public List<ReboStatement> getStatements() {
		return statements;
	}

	public void setStatements(List<ReboStatement> statements) {
		this.statements = statements;
	}



}
