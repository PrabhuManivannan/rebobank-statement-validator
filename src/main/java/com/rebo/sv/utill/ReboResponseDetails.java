package com.rebo.sv.utill;

public enum ReboResponseDetails {
	
	
	TRANSATION_SUCCESS("S001","Found invalid records"),
	INVALID_FILE("E001","Invalid file"),
	
	;
	

	private final String code;

	private final String statusDescription;
	
	
	private ReboResponseDetails(String code, String statusDescription) {
		this.code = code;
		this.statusDescription = statusDescription;
	}


	public String getCode() {
		return code;
	}


	public String getStatusDescription() {
		return statusDescription;
	}


	
}
