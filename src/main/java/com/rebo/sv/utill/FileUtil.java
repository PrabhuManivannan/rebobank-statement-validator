package com.rebo.sv.utill;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 
 * @Author   	Prabhu M
 * @Description singleton file util class 
 */

import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.io.Files;
import com.rebo.sv.api.exception.ReboServiceException;

public class FileUtil {
	
	
	private FileUtil() {
		/*Singleton class*/
	}
	
	public  static boolean isValidFile(final String format) {
		
		return ( !StringUtils.isEmpty(format) &&
				 (format.equalsIgnoreCase("csv")  || format.equalsIgnoreCase("xml")) ) ? true : false ;
		
	}
	
	public  static String getFileName(MultipartFile trxFile) {
		
		return StringUtils.cleanPath(trxFile.getOriginalFilename());
		
	}

	public static String getFileExtension(MultipartFile file) {
		
		
		return  Files.getFileExtension(getFileName(file));
	}
	
	
	public static void storeFile(MultipartFile file,String filePath) throws ReboServiceException {
		
		
		try {
			byte barr[]=file.getBytes();  
			
			BufferedOutputStream bout=new BufferedOutputStream(  
			         new FileOutputStream(filePath+"/"+getFileName(file)));  
			bout.write(barr);  
			bout.flush();  
			bout.close();
		} catch (Exception e) {
			  throw new ReboServiceException(e);
		}
        

	}

}
