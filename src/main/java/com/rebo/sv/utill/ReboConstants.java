package com.rebo.sv.utill;

public abstract class ReboConstants {
	
	public static final String TRANSACTION_FILE="trxFile";
	public static final String URI_ACCESS_INFO="Endpoint could receive eigther XML or CSV files.";
	public static final String REBO_API_INFO="Endpoint could receive eigther XML or CSV files.";
	
	public static final String CSV_FORMAT= "csv";
	public static final String XML_FORMAT= "xml";

	public static final String REFERAANCE_NO = "Reference";
	public static final String ACCOUNT_NUMBER = "AccountNumber";
	public static final String DESCR = "Description";
	public static final String START_BALANCE = "Start Balance";
	public static final String MUTATION = "Mutation";
	public static final String END_BALANCE = "End Balance";

	public static final String[] FILE_HEADER_MAPPING = { REFERAANCE_NO, ACCOUNT_NUMBER, DESCR, START_BALANCE, MUTATION,END_BALANCE };

}
