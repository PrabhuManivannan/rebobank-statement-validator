package com.rebo;
/**
 * 
 * @author   	Prabhu M
 * @Description Spring boot main class to load application context for Rebo Bank Customer Statement Process
 *  
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReboStatementValidatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReboStatementValidatorApplication.class, args);
	}

}
