package com.rebo.sv.core.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rebo.sv.core.ReboResponse;
import com.rebo.sv.model.ReboStatement;
import com.rebo.sv.service.ReboStatementService;
import com.rebo.sv.utill.FileUtil;
import com.rebo.sv.utill.ReboResponseDetails;


@RunWith(SpringRunner.class)
@WebMvcTest(value =  ReboStatementController.class)
public class ReboStatementControllerTest {
	
	 @Autowired
	 private MockMvc mockMvc;
	 
	 @MockBean 
	 ReboStatementService reboStatementService;
	 
	 @MockBean
	 MultipartFile mockMultipartFile;
	
	 @MockBean
	 FileUtil mockFileUtil;
	 
	
	 
	 List<ReboStatement> reboStatements=new ArrayList<>();
	 
	 @Before
	 public void setup() throws Exception{
		 
		   ReboStatement statement = new ReboStatement();
			statement.setTransactionRef(Long.valueOf("123456"));
			statement.setAccountNumber("1000200030005000");
			statement.setDescription("Test transaction jhon pal");
			statement.setEndBalance(new BigDecimal(-44.23).setScale(2, BigDecimal.ROUND_HALF_UP));
			statement.setStartBalance(new BigDecimal(46.6).setScale(2, BigDecimal.ROUND_HALF_UP));
			statement.setMutation(new BigDecimal(-64.83).setScale(2, BigDecimal.ROUND_HALF_UP));
			ReboStatement statement1 = new ReboStatement();
			statement1.setTransactionRef(Long.valueOf("99999"));
			statement1.setAccountNumber("1000200030009000");
			statement1.setDescription("Test transaction jhon pal");
			statement1.setEndBalance(new BigDecimal(-64.23).setScale(2, BigDecimal.ROUND_HALF_UP));
			statement1.setStartBalance(new BigDecimal(34.6).setScale(2, BigDecimal.ROUND_HALF_UP));
			statement1.setMutation(new BigDecimal(-75.83).setScale(2, BigDecimal.ROUND_HALF_UP));
			reboStatements.add(statement);
			reboStatements.add(statement1);
		 
	 }
	 
	 
	 
	 @Test
	 public void testProcessCustomerStatementForValidFile() throws Exception{
		 
		 ReboResponse reboResponse = new ReboResponse();
		 
		 reboResponse.setStatements(reboStatements);
		 reboResponse.setStatusCode(ReboResponseDetails.TRANSATION_SUCCESS.getCode());  
		 reboResponse.setStatusDescription(ReboResponseDetails.TRANSATION_SUCCESS.getStatusDescription());
		
		 MockMultipartFile multipartFile  = new MockMultipartFile("trxFile", "records.csv", "multipart/form-data", "dummymultipartfile".getBytes());
		 when(this.reboStatementService.processStatement(Mockito.any(),Mockito.anyString(),Mockito.anyString())).thenReturn(reboStatements);
		 
		ObjectMapper mapper = new ObjectMapper();
		 ReboResponse expected = mapper.readValue(new ClassPathResource("com/rebo/sv/core/controller/SuccessResponse.json").getFile(),ReboResponse.class);
		 MvcResult result = this.mockMvc.perform(multipart("/rebo/api/validate/statement").file(multipartFile)).andExpect(status().isOk())
				.andReturn();
	    assertEquals(mapper.writeValueAsString(expected), result.getResponse().getContentAsString());
	 }
	 
	 
	 @Test
	 public void testProcessCustomerStatementForInValidFile() throws Exception{
		 
		 ReboResponse reboResponse = new ReboResponse();
		 
		 reboResponse.setStatements(reboStatements);
		 reboResponse.setStatusCode(ReboResponseDetails.TRANSATION_SUCCESS.getCode());  
		 reboResponse.setStatusDescription(ReboResponseDetails.TRANSATION_SUCCESS.getStatusDescription());
		
		 MockMultipartFile multipartFile  = new MockMultipartFile("trxFile", "records.txt", "multipart/form-data", "dummymultipartfile".getBytes());
		 when(this.reboStatementService.processStatement(Mockito.any(),Mockito.anyString(),Mockito.anyString())).thenReturn(reboStatements);
		 
		ObjectMapper mapper = new ObjectMapper();
		 ReboResponse expected = mapper.readValue(new ClassPathResource("com/rebo/sv/core/controller/InvalidFileResponse.json").getFile(),ReboResponse.class);
		 MvcResult result = this.mockMvc.perform(multipart("/rebo/api/validate/statement").file(multipartFile)).andExpect(status().isBadRequest())
				.andReturn();
	    assertEquals(mapper.writeValueAsString(expected), result.getResponse().getContentAsString());
	 }
	 
	 @Test
	 public void testProcessCustomerStatementForException() throws Exception{
		 
		 when(this.reboStatementService.processStatement(Mockito.any(),Mockito.anyString(),Mockito.anyString())).thenThrow(RuntimeException.class);
		 
		 
		 MockMultipartFile multipartFile  = new MockMultipartFile("trxFile", "records.csv", "multipart/form-data", "dummymultipartfile".getBytes());
		 this.mockMvc.perform(multipart("/rebo/api/validate/statement").file(multipartFile)).andExpect(status().is5xxServerError())
				.andReturn();
		 
	 }

}
