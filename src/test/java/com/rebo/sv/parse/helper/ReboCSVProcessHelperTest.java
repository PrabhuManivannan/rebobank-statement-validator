package com.rebo.sv.parse.helper;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.io.ClassPathResource;

import com.rebo.sv.api.exception.FileParserException;
import com.rebo.sv.model.ReboStatement;

@RunWith(MockitoJUnitRunner.class)
public class ReboCSVProcessHelperTest {
	
	@InjectMocks
	ReboCSVProcessHelper testReboCSVProcessHelper;
	
	 List<ReboStatement> reboStatements=new ArrayList<>();
	 
	 @Before
	 public void setup() throws Exception{
		 
		   ReboStatement statement = new ReboStatement();
			statement.setTransactionRef(Long.valueOf("183049"));
			statement.setAccountNumber("NL69ABNA0433647324");
			statement.setDescription("Clothes for Jan King");
			
			statement.setStartBalance(new BigDecimal(86.66).setScale(2, BigDecimal.ROUND_HALF_UP));
			statement.setMutation(new BigDecimal(+44.50).setScale(2, BigDecimal.ROUND_HALF_UP));
			statement.setEndBalance(new BigDecimal(131.16).setScale(2, BigDecimal.ROUND_HALF_UP));
			
			
			ReboStatement statement1 = new ReboStatement();
			statement1.setTransactionRef(Long.valueOf("183356"));
			statement1.setAccountNumber("NL74ABNA0248990274");
			statement1.setDescription("Subscription for Peter de Vries");
			
			statement1.setStartBalance(new BigDecimal(92.98).setScale(2, BigDecimal.ROUND_HALF_UP));
			statement1.setMutation(new BigDecimal(-46.65).setScale(2, BigDecimal.ROUND_HALF_UP));
			statement1.setEndBalance(new BigDecimal(46.33).setScale(2, BigDecimal.ROUND_HALF_UP));
			reboStatements.add(statement);
			reboStatements.add(statement1);
		 					
	 }
	
	@Test
	public void testProcessStatementForCSVParse() throws Exception {
		List<ReboStatement> actual = testReboCSVProcessHelper
				.processStatement(new ClassPathResource("parsetest.csv").getFile());
		assertEquals(reboStatements, actual);
	}

	@Test(expected = FileParserException.class)
	public void testProcessStatementForCSVParseException() throws Exception {

		testReboCSVProcessHelper.processStatement(null);

	}


}
