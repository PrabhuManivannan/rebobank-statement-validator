package com.rebo.sv.parse.helper;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.io.ClassPathResource;

import com.rebo.sv.api.exception.FileParserException;
import com.rebo.sv.model.ReboStatement;


@RunWith(MockitoJUnitRunner.class)
public class ReboXMLProcessHelperTest {
	
	@InjectMocks
	ReboXMLProcessHelper testReboXMLProcessHelper;
	
	 List<ReboStatement> reboStatements=new ArrayList<>();
	 
	@Before
	public void setup() throws Exception{
		
		ReboStatement statement = new ReboStatement();
		statement.setTransactionRef(Long.valueOf("165102"));
		statement.setAccountNumber("NL93ABNA0585619023");
		statement.setDescription("Tickets for Rik Theuß");
		
		statement.setStartBalance(new BigDecimal(3980.00).setScale(2, BigDecimal.ROUND_HALF_UP));
		statement.setMutation(new BigDecimal(+1000.00).setScale(2, BigDecimal.ROUND_HALF_UP));
		statement.setEndBalance(new BigDecimal(4981.00).setScale(2, BigDecimal.ROUND_HALF_UP));
		
		
		ReboStatement statement1 = new ReboStatement();
		statement1.setTransactionRef(Long.valueOf("170148"));
		statement1.setAccountNumber("NL43AEGO0773393871");
		statement1.setDescription("Flowers for Jan Theuß");
		
		statement1.setStartBalance(new BigDecimal(16.52).setScale(2, BigDecimal.ROUND_HALF_UP));
		statement1.setMutation(new BigDecimal(+43.09).setScale(2, BigDecimal.ROUND_HALF_UP));
		statement1.setEndBalance(new BigDecimal(59.61).setScale(2, BigDecimal.ROUND_HALF_UP));
		reboStatements.add(statement);
		reboStatements.add(statement1);
		
	}
	
	@Test
	public void testProcessStatementForParseSuccess() throws Exception{
		
		List<ReboStatement> actual =this.testReboXMLProcessHelper.processStatement(new ClassPathResource("parseXMLTest.xml").getFile());
		assertEquals(reboStatements, actual);
	}
	
	@Test(expected = FileParserException.class)
	public void testProcessStatementForXMLParseException() throws Exception {
		
		//::TODO :: doThrow(RuntimeException.class).when(this.testReboXMLProcessHelper).processStatement(Mockito.any(File.class));
		this.testReboXMLProcessHelper.processStatement(null);

	}



}
