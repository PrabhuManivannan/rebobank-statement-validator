package com.rebo.sv.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doThrow;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;

import com.rebo.sv.api.exception.ReboServiceException;
import com.rebo.sv.model.ReboStatement;
import com.rebo.sv.parse.helper.ReboStatementProcessHelper;

@RunWith(MockitoJUnitRunner.class)
public class ReboStatementServiceImplTest {
	
	@Mock
	ReboStatementProcessHelper reboCSVProcessHelper;
	
	@InjectMocks
	ReboStatementServiceImpl reboStatementServiceImpl;

	@Mock
	ReboStatementProcessHelper reboXMLProcessHelper;
	
	 List<ReboStatement> reboStatements=new ArrayList<>();
	 
	 List<ReboStatement> dublicateStatements=new ArrayList<>();
	 
	 @Before
	 public void setup() throws Exception{
		 
		   ReboStatement statement = new ReboStatement();
			statement.setTransactionRef(Long.valueOf("123456"));
			statement.setAccountNumber("1000200030005000");
			statement.setDescription("Test transaction jhon pal");
			statement.setEndBalance(new BigDecimal(-44.23).setScale(2, BigDecimal.ROUND_HALF_UP));
			statement.setStartBalance(new BigDecimal(46.6).setScale(2, BigDecimal.ROUND_HALF_UP));
			statement.setMutation(new BigDecimal(-64.83).setScale(2, BigDecimal.ROUND_HALF_UP));
			ReboStatement statement1 = new ReboStatement();
			statement1.setTransactionRef(Long.valueOf("99999"));
			statement1.setAccountNumber("1000200030009000");
			statement1.setDescription("Test transaction jhon pal");
			statement1.setEndBalance(new BigDecimal(-64.23).setScale(2, BigDecimal.ROUND_HALF_UP));
			statement1.setStartBalance(new BigDecimal(34.6).setScale(2, BigDecimal.ROUND_HALF_UP));
			statement1.setMutation(new BigDecimal(-75.83).setScale(2, BigDecimal.ROUND_HALF_UP));
			
			
			ReboStatement dubilicateStatement1= new ReboStatement();
			dubilicateStatement1.setTransactionRef(Long.valueOf("99999"));
			dubilicateStatement1.setAccountNumber("1000200030009000");
			dubilicateStatement1.setDescription("Test transaction jhon pal");
			dubilicateStatement1.setEndBalance(new BigDecimal(-64.23).setScale(2, BigDecimal.ROUND_HALF_UP));
			dubilicateStatement1.setStartBalance(new BigDecimal(34.6).setScale(2, BigDecimal.ROUND_HALF_UP));
			dubilicateStatement1.setMutation(new BigDecimal(-75.83).setScale(2, BigDecimal.ROUND_HALF_UP));
			
			ReboStatement dubilicateStatement2 = new ReboStatement();
			dubilicateStatement2.setTransactionRef(Long.valueOf("99999"));
			dubilicateStatement2.setAccountNumber("1000200030009000");
			dubilicateStatement2.setDescription("Test transaction jhon pal");
			dubilicateStatement2.setEndBalance(new BigDecimal(-64.23).setScale(2, BigDecimal.ROUND_HALF_UP));
			dubilicateStatement2.setStartBalance(new BigDecimal(34.6).setScale(2, BigDecimal.ROUND_HALF_UP));
			dubilicateStatement2.setMutation(new BigDecimal(-75.83).setScale(2, BigDecimal.ROUND_HALF_UP));
			
			
			dublicateStatements.add(dubilicateStatement1);
			dublicateStatements.add(dubilicateStatement2);
			reboStatements.add(statement);
			reboStatements.add(statement1);
		 
	 }
	
	@Test
	public void testProcessStatementForCSVSucess() throws Exception {
		
		Resource resource= new ClassPathResource("records.csv");
		MockMultipartFile multipartFile  = new MockMultipartFile( "multipartfile","records.csv","multipart-formdata", resource.getInputStream());
		Mockito.when(this.reboCSVProcessHelper.processStatement(Mockito.any())).thenReturn(reboStatements);
		List<ReboStatement> results=reboStatementServiceImpl.processStatement(multipartFile, "csv","uploads");
		assertThat(reboStatements).isEqualTo(results);
		
	}
	
	@Test
	public void testProcessStatementForXMLSucess() throws Exception {
		
		Resource resource= new ClassPathResource("records.xml");
		MockMultipartFile multipartFile  = new MockMultipartFile( "multipartfile","records.xml","multipart-formdata", resource.getInputStream());
		Mockito.when(this.reboXMLProcessHelper.processStatement(Mockito.any())).thenReturn(reboStatements);
		List<ReboStatement> results=reboStatementServiceImpl.processStatement(multipartFile, "xml","uploads");
		assertThat(reboStatements).isEqualTo(results);
		
	}
	
	
	@Test
	public void testProcessStatementForValidateMutation() throws Exception {
		
		List<ReboStatement> expected=new ArrayList<>();
		
		ReboStatement actaulStatement = new ReboStatement();
		actaulStatement.setTransactionRef(Long.valueOf("99999"));
		actaulStatement.setAccountNumber("1000200030009000");
		actaulStatement.setDescription("Test transaction jhon pal");
		actaulStatement.setEndBalance(new BigDecimal(-64.23).setScale(2, BigDecimal.ROUND_HALF_UP));
		actaulStatement.setStartBalance(new BigDecimal(34.6).setScale(2, BigDecimal.ROUND_HALF_UP));
		actaulStatement.setMutation(new BigDecimal(-75.83).setScale(2, BigDecimal.ROUND_HALF_UP));
		
		expected.add(actaulStatement);

		Resource resource= new ClassPathResource("validateMutation.csv");
		MockMultipartFile multipartFile  = new MockMultipartFile("multipartfile", "validateMutation.csv","multipart-formdata", resource.getInputStream());
		Mockito.when(this.reboCSVProcessHelper.processStatement(Mockito.any())).thenReturn(dublicateStatements);
		List<ReboStatement> results=reboStatementServiceImpl.processStatement(multipartFile, "csv","uploads");
		assertThat(expected).isEqualTo(results);
		
	}
	
	
	
	@Test(expected=ReboServiceException.class)
	public void testProcessStatementForThrowServiceException() throws ReboServiceException, Exception {
		
		Resource resource= new ClassPathResource("records.xml");
		MockMultipartFile multipartFile  = new MockMultipartFile(  "multipartfile","records.xml","multipart-formdata", resource.getInputStream());
		doThrow(RuntimeException.class).when(this.reboXMLProcessHelper).processStatement(Mockito.any(File.class));
		
	   reboStatementServiceImpl.processStatement(multipartFile, "xml","uploads");
	}
	
}
